package com.akbar.powervideoplayer.ui.playeractivity;

import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.os.Handler;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.akbar.powervideoplayer.R;
import com.akbar.powervideoplayer.model.VideoItem;
import com.akbar.powervideoplayer.ui.base.BaseActivity;
import com.akbar.powervideoplayer.utils.AppConstant;
import com.akbar.powervideoplayer.utils.AppUrl;

import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MediaSourceEventListener;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.Locale;


public class PlayerActivity extends BaseActivity implements MediaSourceEventListener, AudioManager.OnAudioFocusChangeListener {

    private SurfaceView svPlayer;
    private ImageButton prev, rew, btnPlay, btnPause, ffwd, next, fullscreen;
    private TextView timeCurrent, playerEndTime;
    private SeekBar mediacontrollerProgress;
    private LinearLayout linMediaController;
    private FrameLayout playerFrameLayout;
    private SimpleExoPlayer exoPlayer;
    private boolean bIsPlaying = false;
    private boolean bControlsActive = true;

    private Handler handler;
    private StringBuilder mFormatBuilder;
    private Formatter mFormatter;
    private DataSource.Factory dataSourceFactory;
    private int position;
    private PlayerViewModel playerViewModel;
    private ArrayList<VideoItem> videoItemList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        initialize();

    }

    private void initialize() {

        svPlayer = findViewById(R.id.sv_player);
        prev = findViewById(R.id.prev);
        rew = findViewById(R.id.rew);
        btnPlay = findViewById(R.id.btnPlay);
        btnPause = findViewById(R.id.btnPause);
        ffwd = findViewById(R.id.ffwd);
        next = findViewById(R.id.next);
        timeCurrent = findViewById(R.id.time_current);
        mediacontrollerProgress = findViewById(R.id.mediacontroller_progress);
        playerEndTime = findViewById(R.id.player_end_time);
        fullscreen = findViewById(R.id.fullscreen);
        linMediaController = findViewById(R.id.lin_media_controller);
        playerFrameLayout = findViewById(R.id.player_frame_layout);

        playerViewModel = ViewModelProviders.of(this).get(PlayerViewModel.class);

        handler = new Handler();
        dataSourceFactory = new DefaultDataSourceFactory(this, Util.getUserAgent(this, "yourApplicationName"),
                new DefaultBandwidthMeter());

        Bundle bundle = getIntent().getExtras();
        String path = null;
        if (bundle != null) {
            path = bundle.getString(AppConstant.PATH, "");
            position = bundle.getInt(AppConstant.POSITION, 0);
        }


        playerViewModel.getVideo().observe(this, new Observer<ArrayList<VideoItem>>() {
            @Override
            public void onChanged(ArrayList<VideoItem> videoItems) {

                videoItemList = videoItems;
                fromSdCard(videoItemList.get(position).getPath());
            }
        });


        /*...... For Online Video ...................*/

       /* initDashPlayer(AppUrl.DASH);
        initHLSPlayer(AppUrl.HLSurl);
        initMp4Player(AppUrl.mp4URL);*/

    }

    private void initMediaControls() {
        initSurfaceView();
        initPlayButton();
        initPauseButton();
        initSeekBar();
        initFwd();
        initPrev();
        initRew();
        initNext();
    }

    private void initNext() {
        next.requestFocus();
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                next();
            }
        });
    }

    private void initRew() {
        rew.requestFocus();
        rew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exoPlayer.seekTo(exoPlayer.getCurrentPosition() - 10000);
            }
        });
    }

    private void initPrev() {
        prev.requestFocus();
        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                previous();
            }
        });
    }

    private void initFwd() {
        ffwd.requestFocus();
        ffwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long targetPosition = exoPlayer.getCurrentPosition() + 10000;
                if (targetPosition >= exoPlayer.getDuration()) {
                    next();
                } else {
                    exoPlayer.seekTo(targetPosition);
                }

            }
        });
    }

    private void initSurfaceView() {
        svPlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleMediaControls();
            }
        });
    }

    private String stringForTime(int timeMs) {
        mFormatBuilder = new StringBuilder();
        mFormatter = new Formatter(mFormatBuilder, Locale.getDefault());
        int totalSeconds = timeMs / 1000;

        int seconds = totalSeconds % 60;
        int minutes = (totalSeconds / 60) % 60;
        int hours = totalSeconds / 3600;

        mFormatBuilder.setLength(0);
        if (hours > 0) {
            return mFormatter.format("%d:%02d:%02d", hours, minutes, seconds).toString();
        } else {
            return mFormatter.format("%02d:%02d", minutes, seconds).toString();
        }
    }

    private void setProgress() {
        mediacontrollerProgress.setProgress(0);
        mediacontrollerProgress.setMax((int) exoPlayer.getDuration() / 1000);

        handler = new Handler();
        //Make sure you update Seekbar on UI thread
        handler.post(new Runnable() {

            @Override
            public void run() {
                if (exoPlayer != null && bIsPlaying) {

                    mediacontrollerProgress.setMax(0);
                    mediacontrollerProgress.setMax((int) exoPlayer.getDuration() / 1000);
                    int mCurrentPosition = (int) exoPlayer.getCurrentPosition() / 1000;
                    mediacontrollerProgress.setProgress(mCurrentPosition);
                    timeCurrent.setText(stringForTime((int) exoPlayer.getCurrentPosition()));
                    playerEndTime.setText(stringForTime((int) exoPlayer.getDuration()));

                    handler.postDelayed(this, 1000);

                }
            }
        });

    }

    private void initSeekBar() {
        mediacontrollerProgress.requestFocus();

        mediacontrollerProgress.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    timeCurrent.setText(stringForTime(progress * 1000));
                    playerEndTime.setText(stringForTime((int) exoPlayer.getDuration()));
                    exoPlayer.seekTo(progress * 1000);
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        mediacontrollerProgress.setMax((int) exoPlayer.getDuration() / 1000);
    }

    private void toggleMediaControls() {

        if (bControlsActive) {
            hideMediaController();
            bControlsActive = false;
        } else {
            showController();
            bControlsActive = true;
            setProgress();
        }

    }

    private void showController() {
        linMediaController.setVisibility(View.VISIBLE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    private void hideMediaController() {
        linMediaController.setVisibility(View.GONE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    private void initPlayButton() {
        btnPlay.requestFocus();
        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                play();
            }
        });
    }

    private void initPauseButton() {

        btnPause.requestFocus();
        btnPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pause();
            }
        });
    }

    private void initExoPlayer(MediaSource mediaSource) {
        if (exoPlayer == null) {
            TrackSelection.Factory videoTrackSelectionFactory =
                    new AdaptiveTrackSelection.Factory(new DefaultBandwidthMeter());
            TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);

            // 2. Create the player
            exoPlayer = ExoPlayerFactory.newSimpleInstance(this, trackSelector);
        }

        exoPlayer.prepare(mediaSource);

        exoPlayer.setVideoSurfaceView(svPlayer);

        exoPlayer.setPlayWhenReady(true);

        initMediaControls();
    }

    private void next() {
        position++;
        if (position > videoItemList.size() - 1) {
            position = 0;
        }
        String path = videoItemList.get(position).getPath();
        MediaSource mediaSource = new ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse(path), handler, this);
        exoPlayer.prepare(mediaSource);
        exoPlayer.seekTo(0);
        setProgress();
    }

    private void previous() {
        position--;
        if (position < 0) {
            position = 0;
        }
        String path = videoItemList.get(position).getPath();
        MediaSource mediaSource = new ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse(path), handler, this);
        exoPlayer.prepare(mediaSource);
        exoPlayer.seekTo(0);
        setProgress();
    }

    private void fromSdCard(String path) {

        if (path.equalsIgnoreCase("")) {
            return;
        }
        //   MediaSource mediaSource = new ExtractorMediaSource(Uri.parse(path), dataSourceFactory, new DefaultExtractorsFactory(), handler, null);
        MediaSource mediaSource = new ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse(path), handler, this);
        initExoPlayer(mediaSource);
    }

    private void play() {
        if (!bIsPlaying) {
            exoPlayer.setPlayWhenReady(true);
            btnPlay.setVisibility(View.GONE);
            btnPause.setVisibility(View.VISIBLE);
            bIsPlaying = true;
            setProgress();
        }
    }

    private void pause() {
        if (bIsPlaying) {
            exoPlayer.setPlayWhenReady(false);
            bIsPlaying = false;
            btnPlay.setVisibility(View.VISIBLE);
            btnPause.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        exoPlayer.release();
    }

    @Override
    protected void onPause() {
        super.onPause();
        pause();
    }

    @Override
    protected void onResume() {
        super.onResume();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                play();
            }
        }, 1000);

    }

    private void initMp4Player(String mp4URL) {

        MediaSource sampleSource =
                new ExtractorMediaSource(Uri.parse(mp4URL), dataSourceFactory, new DefaultExtractorsFactory(),
                        handler, new ExtractorMediaSource.EventListener() {
                    @Override
                    public void onLoadError(IOException error) {

                    }
                });


        initExoPlayer(sampleSource);
    }

    private void initDashPlayer(String dashUrl) {

        MediaSource mediaSource =
                new DashMediaSource(Uri.parse(dashUrl), new DefaultDataSourceFactory(this, AppUrl.USER_AGENT),
                        new DefaultDashChunkSource.Factory(dataSourceFactory), handler,
                        this);

        initExoPlayer(mediaSource);
    }

    private void initHLSPlayer(String dashUrl) {

        MediaSource mediaSource = new HlsMediaSource(Uri.parse(dashUrl), dataSourceFactory, handler, this);

        initExoPlayer(mediaSource);
    }

    @Override
    public void onLoadStarted(DataSpec dataSpec, int dataType, int trackType, Format trackFormat,
                              int trackSelectionReason, Object trackSelectionData, long mediaStartTimeMs,
                              long mediaEndTimeMs, long elapsedRealtimeMs) {

    }

    @Override
    public void onLoadCompleted(DataSpec dataSpec, int dataType, int trackType, Format trackFormat,
                                int trackSelectionReason, Object trackSelectionData, long mediaStartTimeMs,
                                long mediaEndTimeMs, long elapsedRealtimeMs, long loadDurationMs, long bytesLoaded) {

    }

    @Override
    public void onLoadCanceled(DataSpec dataSpec, int dataType, int trackType, Format trackFormat,
                               int trackSelectionReason, Object trackSelectionData, long mediaStartTimeMs,
                               long mediaEndTimeMs, long elapsedRealtimeMs, long loadDurationMs, long bytesLoaded) {

    }

    @Override
    public void onLoadError(DataSpec dataSpec, int dataType, int trackType, Format trackFormat,
                            int trackSelectionReason, Object trackSelectionData, long mediaStartTimeMs,
                            long mediaEndTimeMs, long elapsedRealtimeMs, long loadDurationMs, long bytesLoaded,
                            IOException error, boolean wasCanceled) {

    }

    @Override
    public void onUpstreamDiscarded(int trackType, long mediaStartTimeMs, long mediaEndTimeMs) {

    }

    @Override
    public void onDownstreamFormatChanged(int trackType, Format trackFormat, int trackSelectionReason,
                                          Object trackSelectionData, long mediaTimeMs) {

    }

    @Override
    public void onAudioFocusChange(int focusChange) {

        Toast.makeText(PlayerActivity.this, "focusChange : " + focusChange, Toast.LENGTH_SHORT).show();
        Log.d("TAG", "focusChange :" + focusChange);
        if (focusChange < -1) {
            //LOSS -> PAUSE
            pause();

        } else if (focusChange == -1) {

        } else {
            //GAIN -> PLAY
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    play();
                }
            }, 2000);


        }

    }

}
