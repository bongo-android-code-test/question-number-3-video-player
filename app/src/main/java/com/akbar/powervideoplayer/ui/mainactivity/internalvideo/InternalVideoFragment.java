package com.akbar.powervideoplayer.ui.mainactivity.internalvideo;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.akbar.powervideoplayer.R;
import com.akbar.powervideoplayer.adapter.VideoListAdapter;
import com.akbar.powervideoplayer.model.VideoItem;
import com.akbar.powervideoplayer.ui.base.BaseFragment;
import java.util.ArrayList;

public class InternalVideoFragment extends BaseFragment {

    private InternalVideoViewModel internalVideoViewModel;
    private View rootView;
    private RecyclerView rVFileList;
    private TextView tvEmptyMessage;
    private VideoListAdapter rvFileListAdapter;
    private int columnNumber = 1;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        internalVideoViewModel = ViewModelProviders.of(this).get(InternalVideoViewModel.class);
        rootView = inflater.inflate(R.layout.fragment_internal_videos, container, false);

        initialize();


        return rootView;
    }

    private void initialize() {


        rVFileList = rootView.findViewById(R.id.rVFileList);
        tvEmptyMessage = rootView.findViewById(R.id.tvEmptyMessage);
        rVFileList.setLayoutManager(new GridLayoutManager(getActivity(), columnNumber));
        rVFileList.setItemAnimator(new DefaultItemAnimator());

        internalVideoViewModel.getVideo().observe(this, new Observer<ArrayList<VideoItem>>() {
            @Override
            public void onChanged(ArrayList<VideoItem> videoItems) {

                emptyMessage(videoItems);
                rvFileListAdapter = new VideoListAdapter(getActivity(), videoItems);

                rVFileList.setAdapter(rvFileListAdapter);

            }
        });

    }

    private void emptyMessage(ArrayList<VideoItem> videoItems) {

            if (videoItems.isEmpty()) {
                tvEmptyMessage.setVisibility(View.VISIBLE);
                rVFileList.setVisibility(View.GONE);
            } else {
                tvEmptyMessage.setVisibility(View.GONE);
                rVFileList.setVisibility(View.VISIBLE);
            }

    }


}