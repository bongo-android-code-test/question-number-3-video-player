package com.akbar.powervideoplayer.ui.splashactivity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import com.akbar.powervideoplayer.model.VideoItem;
import com.akbar.powervideoplayer.ui.base.BaseActivity;
import com.akbar.powervideoplayer.ui.mainactivity.MainActivity;
import com.akbar.powervideoplayer.utils.AppConstant;

import androidx.core.app.ActivityCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;


import android.os.Handler;

import com.akbar.powervideoplayer.R;

import java.util.ArrayList;

public class SplashActivity extends BaseActivity {

    private int stayDuration = 1500;
    private SplashViewModel splashViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splah);

        marshmallowPermissions();

    }


    public void marshmallowPermissions() {

        if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {

            if (ActivityCompat.checkSelfPermission(SplashActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                String permissions[] = new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE,};

                requestPermissions(permissions, AppConstant.REQUEST_CODE_FOR_STORAGE);

                return;
            } else {
                gotoMainActivity();
            }

        } else {
            gotoMainActivity();

        }
    }


    private void gotoMainActivity() {
        splashViewModel = ViewModelProviders.of(this).get(SplashViewModel.class);
        splashViewModel.getVideo().observe(this, new Observer<ArrayList<VideoItem>>() {
            @Override
            public void onChanged(ArrayList<VideoItem> videoItems) {

                AppConstant.videoItems = videoItems ;

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();

                    }
                }, stayDuration);
            }
        });


    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (AppConstant.REQUEST_CODE_FOR_STORAGE) {

            case AppConstant.REQUEST_CODE_FOR_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    gotoMainActivity();
                } else {

                    boolean showRationale = true;
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                        for (int i = 0; i < grantResults.length; i++) {
                            showRationale = shouldShowRequestPermissionRationale(permissions[i]);
                            if (showRationale) {
                                break;
                            }
                        }

                        //operations.permissionAccessDeniedDialog(showRationale);
                    }


                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


}
