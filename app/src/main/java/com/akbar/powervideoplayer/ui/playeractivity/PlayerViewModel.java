package com.akbar.powervideoplayer.ui.playeractivity;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.akbar.powervideoplayer.model.VideoItem;
import com.akbar.powervideoplayer.utils.AppConstant;


import java.util.ArrayList;

public class PlayerViewModel extends AndroidViewModel {

    private MutableLiveData<ArrayList<VideoItem>> mText;

    public PlayerViewModel(@NonNull Application application) {
        super(application);
        mText = new MutableLiveData<>();
        mText.setValue(AppConstant.videoItems);
    }

    public LiveData<ArrayList<VideoItem>> getVideo() {
        return mText;
    }
}