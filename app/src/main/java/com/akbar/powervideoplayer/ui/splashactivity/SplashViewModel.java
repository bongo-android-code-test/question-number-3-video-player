package com.akbar.powervideoplayer.ui.splashactivity;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.akbar.powervideoplayer.model.VideoItem;
import com.akbar.powervideoplayer.utils.Operations;

import java.util.ArrayList;

public class SplashViewModel extends AndroidViewModel {

    private MutableLiveData<ArrayList<VideoItem>> videos;

    public SplashViewModel(@NonNull Application application) {
        super(application);
        videos = new MutableLiveData<>();
        videos.setValue(Operations.getVideoList(application));
    }

    public LiveData<ArrayList<VideoItem>> getVideo() {
        return videos;
    }
}
