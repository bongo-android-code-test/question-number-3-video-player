package com.akbar.powervideoplayer.ui.mainactivity.internalvideo;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.akbar.powervideoplayer.model.VideoItem;
import com.akbar.powervideoplayer.utils.AppConstant;

import java.util.ArrayList;

public class InternalVideoViewModel extends AndroidViewModel {

    private MutableLiveData<ArrayList<VideoItem>> videos;

    public InternalVideoViewModel(@NonNull Application application) {
        super(application);
        videos = new MutableLiveData<>();
        videos.setValue(AppConstant.videoItems);
    }

    public LiveData<ArrayList<VideoItem>> getVideo() {
        return videos;
    }
}