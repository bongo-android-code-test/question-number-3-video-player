package com.akbar.powervideoplayer.utils;

import com.akbar.powervideoplayer.model.VideoItem;

import java.util.ArrayList;

public class AppConstant {


    public static ArrayList<VideoItem> videoItems = new ArrayList<>();

    public static final String PATH = "path";
    public static final String POSITION = "position";
    public static final int REQUEST_CODE_FOR_STORAGE = 101;
}
