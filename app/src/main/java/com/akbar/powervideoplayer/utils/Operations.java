package com.akbar.powervideoplayer.utils;


import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import com.akbar.powervideoplayer.model.VideoItem;

import java.util.ArrayList;

public class Operations {
    private Context context;
    private ProgressDialog progressDialog;

    public Operations(Context context) {
        this.context = context;
        progressDialog = new ProgressDialog(context);
    }

    public void buildProgressDialog(String title, String message, boolean setCancelable) {
        try {
            if (!progressDialog.isShowing()) {
                progressDialog.setTitle(title);
                progressDialog.setMessage(message);
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setCancelable(setCancelable);
                progressDialog.show();
            }
        } catch (Exception e) {
            Log.e("buildProgressError", e.toString());
        }
    }

    public void dismissProgressDialog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                try {
                    progressDialog.dismiss();
                } catch (Exception e) {
                    Log.e("dismissProgressError", e.toString());
                }
            }
        }
    }


    static public ArrayList<VideoItem> getVideoList(Context context) {

        ArrayList<VideoItem> videoItems = new ArrayList<>();

        String[] videoProjection = {

                MediaStore.Video.Media.ARTIST,
                MediaStore.Video.Media.ALBUM,
                MediaStore.Video.Media.TITLE,
                MediaStore.Video.Media.DURATION,
                MediaStore.Video.Media._ID,
                MediaStore.Video.Media.DISPLAY_NAME,
                MediaStore.Video.Media.DATA
        };


        Uri videoUriForExternalStorage = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;


        Cursor videoCursorForExternalStorage = context.getContentResolver().query(videoUriForExternalStorage, videoProjection, null, null, null);

        while (videoCursorForExternalStorage.moveToNext()) {

            VideoItem videoItem = new VideoItem();

            String title = videoCursorForExternalStorage.getString(videoCursorForExternalStorage.getColumnIndexOrThrow(MediaStore.Video.Media.TITLE));
            String artist = videoCursorForExternalStorage.getString(videoCursorForExternalStorage.getColumnIndexOrThrow(MediaStore.Video.Media.ARTIST));
            String album = videoCursorForExternalStorage.getString(videoCursorForExternalStorage.getColumnIndexOrThrow(MediaStore.Video.Media.ALBUM));
            long duration = videoCursorForExternalStorage.getLong(videoCursorForExternalStorage.getColumnIndexOrThrow(MediaStore.Video.Media.DURATION));
            String data = videoCursorForExternalStorage.getString(videoCursorForExternalStorage.getColumnIndexOrThrow(MediaStore.Video.Media.DATA));
            long videoId = videoCursorForExternalStorage.getLong(videoCursorForExternalStorage.getColumnIndexOrThrow(MediaStore.Video.Media._ID));

            if (duration > 1000) {
                videoItem.setTitle(title);
                videoItem.setAlbum(album);
                videoItem.setArtist(artist);
                videoItem.setDuration(duration);
                videoItem.setPath(data);
                videoItem.setVideoId(videoId);
                videoItems.add(videoItem);
            }
        }


        return videoItems;
    }


    public static String getDuration(long milliseconds) {
        long sec = (milliseconds / 1000) % 60;
        long min = (milliseconds / (60 * 1000)) % 60;
        long hour = milliseconds / (60 * 60 * 1000);

        String s = (sec < 10) ? "0" + sec : "" + sec;
        String m = (min < 10) ? "0" + min : "" + min;
        String h = "" + hour;

        String time = "";
        if (hour > 0) {
            time = h + ":" + m + ":" + s;
        } else {
            time = m + ":" + s;
        }
        return time;
    }


}
