package com.akbar.powervideoplayer.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.akbar.powervideoplayer.R;
import com.akbar.powervideoplayer.model.VideoItem;
import com.akbar.powervideoplayer.ui.playeractivity.PlayerActivity;
import com.akbar.powervideoplayer.utils.AppConstant;
import com.akbar.powervideoplayer.utils.Operations;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class VideoListAdapter extends RecyclerView.Adapter<VideoListAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<VideoItem> videoItems;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView ivVideoImage;
        public TextView tvVideoName, tvDuration;
        public LinearLayout mainLayout;

        public MyViewHolder(View view) {
            super(view);

            ivVideoImage = view.findViewById(R.id.ivVideoImage);
            tvVideoName = view.findViewById(R.id.tvVideoName);
            tvDuration = view.findViewById(R.id.tvDuration);
            mainLayout = view.findViewById(R.id.mainLayout);

        }
    }

    public VideoListAdapter(Context context, ArrayList<VideoItem> itemArrayList) {
        this.context = context;
        this.videoItems = itemArrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_video, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder itemHolder, final int position) {

        final VideoItem videoItem = videoItems.get(position);

        itemHolder.tvVideoName.setText(videoItem.getTitle());
        itemHolder.tvDuration.setText(Operations.getDuration(videoItem.getDuration()));

        Glide.with(itemHolder.ivVideoImage.getContext())
                .load(videoItem.getPath()) // or URI/path
                .into(itemHolder.ivVideoImage); //imageview to set thumbnail to

        itemHolder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PlayerActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(AppConstant.PATH, videoItem.getPath());
                bundle.putInt(AppConstant.POSITION, position);
                intent.putExtras(bundle);
                context.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return videoItems.size();
    }


}
